function DOM_ID(id) {
  return document.getElementById(id);
}

var numArr = [];

// THÊM SỐ
document.querySelector("#btnAddNumber").onclick = function () {
  var number = Number(DOM_ID("number").value);
  DOM_ID("number").value = "";
  numArr.push(number);
  DOM_ID("resultArr").innerText = numArr;
};

// BÀI 1: Tổng số dương
document.querySelector("#btnBai1").onclick = function () {
  var tong = 0;
  numArr.forEach(function (number) {
    if (number > 0) {
      tong += number;
    }
  });
  DOM_ID("resultBai1").innerText = `Tổng số dương: ${tong}`;
};

// BÀI 2: Đếm số dương
document.querySelector("#btnBai2").onclick = function () {
  var count = 0;
  numArr.forEach(function (number) {
    if (number > 0) {
      count++;
    }
  });
  DOM_ID("resultBai2").innerText = `Đếm số dương: ${count}`;
};

// BÀI 3: Tìm số nhỏ nhất
document.querySelector("#btnBai3").onclick = function () {
  var minNum = numArr[0];
  numArr.forEach(function (number) {
    if (number < minNum) {
      minNum = number;
    }
  });
  DOM_ID("resultBai3").innerText = `Số nhỏ nhất: ${minNum}`;
};

// BÀI 4: Tìm số dương nhỏ nhất
document.querySelector("#btnBai4").onclick = function () {
  var mangSoDuong = [];
  numArr.forEach(function (number) {
    if (number > 0) {
      mangSoDuong.push(number);
    }
  });
  var minDuong = mangSoDuong[0];
  mangSoDuong.forEach(function (number) {
    if (number < minDuong) {
      minDuong = number;
    }
  });
  DOM_ID("resultBai4").innerText = `Số dương nhỏ nhất: ${minDuong}`;
};

// BÀI 5: Tìm số chẵn cuối cùng
document.querySelector("#btnBai5").onclick = function () {
  var soChanLast = -1;
  numArr.forEach(function (number) {
    if (number % 2 == 0) {
      soChanLast = number;
    }
  });
  DOM_ID("resultBai5").innerText = `Số chẵn cuối cùng: ${soChanLast}`;
};

// BÀI 6: Đổi chỗ giá trị theo 2 vị trí
document.querySelector("#btnBai6").onclick = function () {
  var viTri1 = DOM_ID("viTri1Bai6").value;
  var viTri2 = DOM_ID("viTri2Bai6").value;
  var temp = numArr[viTri1];
  numArr[viTri1] = numArr[viTri2];
  numArr[viTri2] = temp;
  DOM_ID("resultBai6").innerText = `Mảng sau khi đổi: ${numArr}`;
};

// BÀI 7: Sắp xếp mảng tăng dần
document.querySelector("#btnBai7").onclick = function () {
  numArr.sort(function (a, b) {
    return a - b;
  });
  DOM_ID("resultBai7").innerText = `Mảng sau khi sắp xếp: ${numArr}`;
};

// BÀI 8: Tìm số nguyên tố đầu tiên
document.querySelector("#btnBai8").onclick = function () {
  var result = -1;
  for (var i = 0; i < numArr.length; i++) {
    if (checkSoNguyenTo(numArr[i])) {
      result = numArr[i];
      break;
    }
  }
  DOM_ID("resultBai8").innerText = `Số nguyên tố đầu tiên: ${result}`;
};
function checkSoNguyenTo(num) {
  var check = true;
  if (num <= 1) {
    check = false;
    return check;
  } else {
    for (var i = 2; i <= Math.sqrt(num); i++) {
      if (num % i == 0) {
        check = false;
        return check;
      }
    }
  }
  return check;
}

// BÀI 9: Nhập thêm mảng số thực, đếm số nguyên
var soThucArr = [];
document.querySelector("#btnAddNumberBai9").onclick = function () {
  var numSoThuc = Number(DOM_ID("numberBai9").value);
  DOM_ID("numberBai9").value = "";
  soThucArr.push(numSoThuc);
  DOM_ID("resultArrBai9").innerText = soThucArr;
};
document.querySelector("#btnBai9").onclick = function () {
  var count = 0;
  soThucArr.forEach(function (num) {
    if (Number.isInteger(num)) {
      count++;
    }
  });
  DOM_ID("resultBai9").innerText = `Đếm số nguyên: ${count}`;
};

// BÀI 10: So sánh số lượng số dương và số âm
document.querySelector("#btnBai10").onclick = function () {
  var countSoDuong = 0;
  var countSoAm = 0;
  var result = "";
  numArr.forEach(function (num) {
    if (num != 0) {
      if (num > 0) {
        countSoDuong++;
      } else {
        countSoAm++;
      }
    }
  });
  if (countSoDuong > countSoAm) {
    result = "Số dương > Số âm";
  } else if (countSoDuong < countSoAm) {
    result = "Số dương < Số âm";
  } else {
    result = "Số dương = Số âm";
  }
  DOM_ID("resultBai10").innerText = `${result}`;
};
